using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;



public class CFManager : MonoBehaviour
{
    public InputField inputField;
    public Text resultText;

    private string correctNumber = "231101";

    public void CheckNumber()
    {
        string input = inputField.text;
        if (input == correctNumber)
        {
            resultText.text = "Ganaste";
        }
        else
        {
            resultText.text = "Perdiste";
        }
    }
}


