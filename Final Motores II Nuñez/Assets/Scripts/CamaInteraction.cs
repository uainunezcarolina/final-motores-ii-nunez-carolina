using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CamaInteraction : MonoBehaviour
{
    public GameObject interactionText;
    public string nextSceneName = "Menu sue�os";
    private bool isMouseOver;

    private void OnMouseEnter()
    {
        isMouseOver = true;
        interactionText.SetActive(true);
    }

    private void OnMouseExit()
    {
        isMouseOver = false;
        interactionText.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            interactionText.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            interactionText.SetActive(false);
        }
    }

    void Update()
    {
        if (isMouseOver && Input.GetKeyDown(KeyCode.E))
        {
            SceneManager.LoadScene(nextSceneName);
        }
    }
}
