using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using TMPro;

public class ManagerPuzzle : MonoBehaviour
{
    [SerializeField] private Transform juegotransform;
    [SerializeField] private Transform piezaprefab;

    private List<Transform> piezas;
    private int emptylocation;
    private int size;
    private bool barajar=false;

    private void crearPiezas(float gapthickness)
    {
        float ancho = 1 / (float) size;
        for (int fila = 0; fila < size; fila++)
        {
            for (int col = 0; col < size; col++)
            {
                Transform pieza = Instantiate(piezaprefab, juegotransform);
                piezas.Add(pieza);
                pieza.localPosition = new Vector3(-1+(2*ancho*col) + ancho,
                                                  +1-(2*ancho*fila)-ancho,
                                                  0);
                pieza.localScale = ((2 * ancho) - gapthickness) * Vector3.one;
                pieza.name = $"{(fila * size) + col}";

                if ((fila==size -1) && (col == size - 1))
                {
                    emptylocation = (size * size) - 1;
                    pieza.gameObject.SetActive(false);
                } else
                {
                    float gap = gapthickness / 2;
                    Mesh mesh = pieza.GetComponent<MeshFilter>().mesh;
                    Vector2[] uv = new Vector2[4];
                    //orden coord. uv: (0,1), (1,1),(0,0),(1,0)
                    uv[0] = new Vector2((ancho*col)+gap, 1 -((ancho*(fila+1))-gap));
                    uv[1] = new Vector2((ancho*(col+1))-gap,1-((ancho *(fila +1))-gap));
                    uv[2] = new Vector2((ancho *col)+gap,1-((ancho*fila)+gap));
                    uv[3] = new Vector2((ancho*(col+1))-gap,1-((ancho*fila)+gap));

                    mesh.uv = uv;

                }
            }
        }
    }

    void Start()
    {
        piezas = new List<Transform>();
        size = 4;
        crearPiezas(0.01f);
    }

    void Update()
    {
        if (!barajar && CheckComplemento())
        {
            barajar = true;
           StartCoroutine(esperabaraja(0f));

        }
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit)
            {
                for (int i = 0; i < piezas.Count; i++)
                {
                    if (piezas [i] == hit.transform)
                    {
                        if (SwapValido(i, -size, size)) { break; }
                        if (SwapValido(i, +size, size)) { break; }
                        if (SwapValido(i, -1, 0)) { break; }
                        if (SwapValido(i, +1, size - 1)) { break; }
                    }
                }
            }
        }
        tiempoActual+=Time.deltaTime;
        if (tiempoActual >= tiempoObjetivo)
        {
            textoWin.SetActive(true);
        }
            
}

    private float tiempoActual;
    private float tiempoObjetivo = 300f;
    public GameObject textoWin;
 
    private bool SwapValido(int i, int desplazar, int colCheck)
    {
        if (((i % size) != colCheck) && ((i + desplazar) == emptylocation))
        {
            (piezas[i], piezas[i + desplazar]) = (piezas[i + desplazar], piezas[i]);
            (piezas[i].localPosition, piezas[i + desplazar].localPosition) = ((piezas[i + desplazar].localPosition, piezas[i].localPosition));
            emptylocation = i;
            return true;
        }
        return false;
    }
    private bool CheckComplemento()
    {
        for (int i = 0; i < piezas.Count; i++)
        {
            if (piezas[i].name != $"{i}")
            {
                return false;
            }
        }
        return true;
    }
    private IEnumerator esperabaraja(float duracion)
    {
        yield return new WaitForSeconds(duracion);
        Baraja();
        barajar= false;
    }
    private void Baraja()
    {
        int contador = 0;
        int ult = 0;
        while (contador < (size * size * size))
        {
            // elige un lugar random
            int rnd = Random.Range(0, size * size);
            if (rnd == ult) { continue; }
            ult = emptylocation;
            if (SwapValido(rnd, -size, size))
            {
                contador++;
            }
            else if (SwapValido(rnd, +size, size))
            {
                contador++;
            }
            else if (SwapValido(rnd, -1, 0))
            {
                contador++;
            }
            else if (SwapValido(rnd, +1, size - 1))
            {
                contador++;
            }
        }
    }


}
