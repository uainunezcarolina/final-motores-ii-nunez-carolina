using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MostrarImagen : MonoBehaviour
{
    public Image imageToShow;
    public Button hideButton;

    public void Show()
    {
        imageToShow.gameObject.SetActive(true);
        hideButton.gameObject.SetActive(true);
    }

    public void Hide()
    {
        imageToShow.gameObject.SetActive(false);
        hideButton.gameObject.SetActive(false);
    }
}
