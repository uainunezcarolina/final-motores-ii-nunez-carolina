using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.Rendering;

public class MovimientoJugador : MonoBehaviour
{
    public float senx;
    public float seny;

    public Transform orientacion;
    float xrotation;
    float yrotation;

    void Start()
    {
       Cursor.lockState = CursorLockMode.Locked;
       Cursor.visible = false;
    }

    // Update is called once per frame
    void Update()
    {
        float mousex = Input.GetAxisRaw("Mouse X") * Time.deltaTime * senx;
        float mousey = Input.GetAxisRaw("Mouse Y") * Time.deltaTime * seny;
        yrotation += mousex;
        xrotation -= mousey;
        xrotation = Mathf.Clamp(xrotation, -90f,90f);

        transform.rotation = Quaternion.Euler(xrotation, yrotation, 0);
        orientacion.rotation = Quaternion.Euler(0, yrotation, 0);


    }
}
