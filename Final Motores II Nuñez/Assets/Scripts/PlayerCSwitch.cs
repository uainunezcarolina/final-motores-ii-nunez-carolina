using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerCSwitch : MonoBehaviour
{
    public float jumpforce = 10f;
    public Rigidbody2D rb;
    public string colorActual;
    public SpriteRenderer sr;

    public Color colorAmarillo;
    public Color colorAzul;
    public Color colorRojo; 
    public Color colorVioleta;

    private void Start()
    {
        setColorRandom();
    }
    void Update()
    {
        if (Input.GetButtonDown("Jump") || Input.GetMouseButtonDown(0))
        {
            rb.velocity = Vector2.up * jumpforce;
        }

    }
    void OnTriggerEnter2D(Collider2D col)
    {

        if (col.tag == "cambiadorColor")
        {
            setColorRandom();
            Destroy(col.gameObject);
            return;
        }

      if (col.tag!= colorActual)
        {
            Debug.Log("Game Over");
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    void setColorRandom()
    {
        int index = Random.Range(0, 4);
        switch (index)
        {
            case 0: colorActual = "rojo";
                sr.color=colorRojo;
                break;
            case 1: colorActual = "amarillo";
                sr.color = colorAmarillo;
                break ;
            case 2: colorActual = "azul";
                sr.color = colorAzul;
                break;
            case 3: colorActual = "violeta";
                sr.color = colorVioleta;
            break;

        } 
    }
}
