using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMov : MonoBehaviour
{
    [Header("Movimiento")]

    public float moveSpeed;
    public float groundDrag;

    [Header("Ground Check")]
    public float playerHeight;
    public LayerMask whatIsGround;
    bool grounded;

    public Transform orientation;

    float horizontalInput;
    float verticalInput;

    Vector3 movedirection;
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        rb.freezeRotation = true;
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void myInput()
    {
        horizontalInput = Input.GetAxisRaw("Horizontal");
        verticalInput = Input.GetAxisRaw("Vertical");

    }

    private void FixedUpdate()
    {
        playerMovement();
    }

    private void playerMovement()
    {
        movedirection = orientation.forward * verticalInput + orientation.right * horizontalInput;
        rb.AddForce(movedirection.normalized*moveSpeed*10f,ForceMode.Force);
    }

    private void speedControl()
    {
        Vector3 flatvel = new Vector3(rb.velocity.x,0f,rb.velocity.z);
        if(flatvel.magnitude> moveSpeed)
        {
            Vector3 limitedvel = flatvel.normalized * moveSpeed;
            rb.velocity=new Vector3(limitedvel.x, limitedvel.y, limitedvel.z);

        }
    }

    void Update()
    {
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.2f, whatIsGround);

        myInput();
        speedControl();

        if (grounded)
        {
            rb.drag = groundDrag;
        }else
        {
            rb.drag = 0;
        }
    }
}
